
###############################################################
# Mouse Position Tracker
#	by Zach Holland
#
#	Class python file for automating pretty much any computer task
#	requiring clicking. This program will allow the user to save
#	the mouse coordinates to a text file, which can then be used
#	to automate the task at hand.
import pyautogui, sys
import ctypes
import win32api,pickle
import time
from pywinauto.findwindows    import find_window
from pywinauto.win32functions import SetForegroundWindow


class MouseClickTracker:
	
	def __init__(self):
		print("***Mouse Position Tracker Initiated***")
		print("-->Continue clicking until you have completed your task")
		print("-->Do not type anything or click enter to complete the window")
		self.filehandler = open('Mouse_Click_Positions.txt', 'a')
		SetForegroundWindow(find_window(title='ArcMap*32.exe'))

	###############################################################
	# GetPosition
	#	using the pyautogui get the position of the mouse and save
	#	it as a string and return it
	def GetPosition(self):
		x, y = pyautogui.position()
		#positionStr = 'X: ' + str(x).rjust(4) + ' Y: ' + str(y).rjust(4)
		#print(positionStr)
		positionStr = str(x)+" "+str(y)+" "
		return positionStr
	###############################################################
	# DetectClick
	#	using win32api we can determine if the mouse has been clicked
	#	and whether or not it is a left or right click. Then we append
	#	it to a text file along with the mouse position for future use
	def DetectClick(self):
		state_left = win32api.GetKeyState(0x01)  # Left button down = 0 or 1. Button up = -127 or -128
		state_right = win32api.GetKeyState(0x02)  # Right button down = 0 or 1. Button up = -127 or -128

		while True:
			a = win32api.GetKeyState(0x01)
			b = win32api.GetKeyState(0x02)

			if a != state_left:  # Button state changed
				state_left = a
				if a < 0:
					print('Left Button Pressed @ ',MouseClickTracker.GetPosition(self))
					self.filehandler.write('left '+ MouseClickTracker.GetPosition(self)+'\n')
				else:
					print('Left Button Released')

			if b != state_right:  # Button state changed
				state_right = b
				if b < 0:
					print('Right Button Pressed @ ',MouseClickTracker.GetPosition(self))
					self.filehandler.write('right '+ MouseClickTracker.GetPosition(self)+'\n')
				else:
					print('Right Button Released')
			time.sleep(0.001)


if __name__ == '__main__':
	try:
		valid = True
		mouseObject = MouseClickTracker()
		while valid == True:
			mouseObject.DetectClick()
	except KeyboardInterrupt:
		print("Terminated!")