########################################################################
# Cursor.py
#
#	-Allows 
#
########################################################################
#	The Original cursor program
#
#	while True:
#		x, y = pyautogui.position()
#		event = pygame.event.poll()
#		positionStr = 'X: ' + str(x).rjust(4) + ' Y: ' + str(y).rjust(4)
#		print(positionStr, end='')
#		print('\b' * len(positionStr), end='', flush=True) 
#	
########################################################################		

import pyautogui
from tkinter import *
import pygame
pygame.init()
print('Press Ctrl-C to quit.')
#event = pygame.event.poll()
###################################################
# key
###################################################	
#	Module that tells you the key that was pressed
#	by the user
#
def key(event):
	# Print out the key that was pressed
	""" print ("pressed", repr(event.char))"""
	# If q is pressed close the program
	if event.char == 'q' :
		sys.exit(0)
		
###################################################
# callback
###################################################	
#	Module that tells you the where you clicked on
#	the screen. Due to the limitations of the frame
#	bar on top and the outer rims, I have added in
#	values to represent the difference in position
#	for the mouse position
#		x + 8	|	y+30
#	
def callback(event):
	frame.focus_set()
	# Coordinates within the frame
	print ("Clicked on the screen at: ", event.x+8, (event.y+30))
	# Coordinates within the actual screen resolution
	a, b = pyautogui.position()
	positionStr = 'X: ' + str(a).rjust(4) + ' Y: ' + str(b).rjust(4)
	print(positionStr, end='')
	print('\b' * len(positionStr), end='', flush=True) 

try:
	# Create a Tinkter object named root, that is transparent
	root = Tk()
	root.attributes('-alpha',0.3)

	###################################################
	# frame
	###################################################
	#	These lines utilize the tkinter module to create
	#	a frame widget that binds the key and left mouse
	#	click to the widget. It then runs the a loop until
	#	it is closed by the user.
	
	frame = Frame(root, width=1366, height=768)
	frame.bind("<Key>", key)
	frame.bind("<Button-1>", callback)
	frame.pack()
	root.mainloop()

	
except KeyboardInterrupt:
	print('\nDone.')
	print('\b' * len(positionStr), end='', flush=True)

print(pyautogui.size())
#width,height = pyautogui.size()
