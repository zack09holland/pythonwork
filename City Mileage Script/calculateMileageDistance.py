########################################################################
# calculateMileageDistance
#
#
#
#
########################################################################
import getpass
import os, time
import openpyxl
import re
from geopy.geocoders import Nominatim
from geopy.distance import vincenty


class calculateMileageDistance:
    def __init__(self):
        # List of sub cities we are comparing to the main ones
        self.cityList = []
        # Main cities we are calculating distances too
        self.MorrisDistances = []
        self.LakeCountyDistances = []
        self.DownersGroveDistances = []
        self.CrystalLakeDistances = []
        self.JoiletDistances = []
        self.KendallCountyDistances = []
        self.StCharlesDistances = []
        # Desktop path to save the excel file to if needed
        #self.desktopPath = os.path.join('C:\\Users\\' + getpass.getuser() + '\\Desktop\\')
        #File location we will parse, populate with data, and then save to
        self.fileLocation = input("Drag the excel file containing the data to the cmd(press enter when done)\n:")

    ########################################################################
    # Process Excel data sheet
    #
    def parseExcel(self):
        # Load the excel file to parse the data from
        wb = openpyxl.load_workbook(self.fileLocation)
        # Get the excel sheet that is currently open(in this case we only have 1)
        dataSheet = wb.get_active_sheet()
        print (dataSheet.max_row)
        # Go through the cities column of data and append the values to the unique list
        for row in range(2, dataSheet.max_row+1):
            #Since the city names are contained in a web address we have to use regular
            #expressions to get the part we need
            reCity = re.findall('.com\/(.*?)\/', dataSheet['{}{}'.format('E', row)].value)
            # Replace the '-' in the city names with a space and then append it to the cityList
            cityName = str(reCity).replace("-", " ").replace(" il", " illinois")
            self.cityList.append(cityName)
            #print (cityName)

    ########################################################################
    # Calculate distances given the list of city names
    #
    def calculateDistance(self, cityList):
        print("Getting coordinates...")
        #Utilizing geopy we can obtain the longitude and latitude by first createing a geolocator object
        geolocator = Nominatim()
        # Get distances to the main cities
        #   ***Since the geolocator function only allows 1 quesry every second, I have put in 1second break
        #      between each geolocator query
        morrisLocation = (geolocator.geocode('morris illinois').latitude, geolocator.geocode('morris illinois').longitude)
        time.sleep(1)
        lakeCountyLocation = (geolocator.geocode('lake county illinois').latitude, geolocator.geocode('lake county illinois').longitude)
        time.sleep(1)
        downersGroveLocation = (geolocator.geocode('downers grove illinois').latitude, geolocator.geocode('downers grove illinois').longitude)
        time.sleep(1)
        crystalLakeLocation = (geolocator.geocode('crystal lake illinois').latitude, geolocator.geocode('crystal lake illinois').longitude)
        time.sleep(1)
        joiletLocation = (geolocator.geocode('joliet illinois').latitude, geolocator.geocode('joliet illinois').longitude)
        time.sleep(1)
        kendallCountyLocation = (geolocator.geocode('kendall county illinois').latitude, geolocator.geocode('kendall county illinois').longitude)
        time.sleep(1)
        stCharlesLocation = (geolocator.geocode('st charles illinois').latitude, geolocator.geocode('st charles illinois').longitude)

        print("Calculating distances now...")
        # Calculate the distance using the vincenty function that measures the distance between two sets of coordinates
        for i in cityList:
            subCitylocation = (geolocator.geocode(str(i)).latitude, geolocator.geocode(str(i)).longitude)
            #print(i,": ", subCitylocation)
            #print("Compare Main City: ", morrisLocation)
            # Calculate distance for the subcity to the main city and then append it to the corresponding list
            tempValue = int(vincenty(subCitylocation, morrisLocation).miles)
            self.MorrisDistances.append(tempValue)

            tempValue = int(vincenty(subCitylocation, lakeCountyLocation).miles)
            self.LakeCountyDistances.append(tempValue)

            tempValue = int(vincenty(subCitylocation, downersGroveLocation).miles)
            self.DownersGroveDistances.append(tempValue)

            tempValue = int(vincenty(subCitylocation, crystalLakeLocation).miles)
            self.CrystalLakeDistances.append(tempValue)

            tempValue = int(vincenty(subCitylocation, joiletLocation).miles)
            self.JoiletDistances.append(tempValue)

            tempValue = int(vincenty(subCitylocation, kendallCountyLocation).miles)
            self.KendallCountyDistances.append(tempValue)

            tempValue = int(vincenty(subCitylocation, stCharlesLocation).miles)
            self.StCharlesDistances.append(tempValue)
        print("Finished calculations!")

    ########################################################################
    # Repopulate excel file with new calculated distances
    #
    def populateExcel(self):
        print("Populating Excel...")
        # Load the excel file to parse the data from
        wb = openpyxl.load_workbook(self.fileLocation)
        # Get the excel sheet that is currently open
        dataSheet = wb.get_active_sheet()

        for row in range(2, dataSheet.max_row):
            #Since we start on row 2, and the index of a list starts at 0
            #we have to subtract 2 from the row to obtain the corresponding distance
            dataSheet['{}{}'.format('G', row)].value = self.MorrisDistances[row-2]
            dataSheet['{}{}'.format('H', row)].value = self.LakeCountyDistances[row-2]
            dataSheet['{}{}'.format('I', row)].value = self.DownersGroveDistances[row-2]
            dataSheet['{}{}'.format('J', row)].value = self.CrystalLakeDistances[row-2]
            dataSheet['{}{}'.format('K', row)].value = self.JoiletDistances[row-2]
            dataSheet['{}{}'.format('L', row)].value = self.KendallCountyDistances[row-2]
            dataSheet['{}{}'.format('M', row)].value = self.StCharlesDistances[row-2]

        wb.save(self.fileLocation)
        print("Excel has been populated, and saved!")


if __name__ == '__main__':
    cityData = calculateMileageDistance()
    cityData.parseExcel()
    cityData.calculateDistance(cityData.cityList)
    cityData.populateExcel()

